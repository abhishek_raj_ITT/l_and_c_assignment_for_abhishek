package assignment3;//providing space between package and import

import java.io.IOException;//closely related imports are grouped together.
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Blogs {
	// providing space between import and class and following one tab of space.
	public String getJsonString() {
		try {

			System.out.println("Enter blog name: ");
			Scanner scan = new Scanner(System.in);
			String blogName = scan.next();
			System.out.println("\nEnter The Range : ");
			String rangetext = scan.next("[0-9]*-[0-9]*");
			String[] numbers = rangetext.split("-");
			int startValue = Integer.parseInt(numbers[0]);
			int endValue = Integer.parseInt(numbers[1]);
			if (endValue > 50) {
				//providing space between opening brackets and if statements.
				throw new OutOfRangeException("Please enter upper limit less than or equal to 50");
			}//providing one line of space at the end of block to provide readabilty.

			String inline = "";
			String out = "";
			URL url = new URL("https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + endValue + "&start="
					+ (startValue - 1));

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.connect();
			Scanner scanner = new Scanner(url.openStream());
			while (scanner.hasNext()) {
				inline += scanner.nextLine();
			}
			scanner.close();
			while (inline.startsWith("var tumblr_api_read = ")) {
				out = inline.substring("var tumblr_api_read = ".length());
				out = out.replace(";", "");
				break;

			}
			return out;
		} catch (OutOfRangeException ex) {
			System.out.println(ex.getMessage());
			System.exit(0);
		}

		catch (IOException e) {
			e.printStackTrace();
		} catch (InputMismatchException exception) {
			System.out.println("please provide valid input range");

		}

		return null;

	}

	public void printValues(String out) {
		try {
			JSONObject jsonBlogObject = new JSONObject(out);
			JSONObject jsonkeyObject = new JSONObject(jsonBlogObject.getJSONObject("tumblelog").toString());
			System.out.println("title :" + jsonkeyObject.get("title").toString());
			System.out.println();
			System.out.println("Description : " + jsonkeyObject.get("description").toString());
			System.out.println("Name : " + jsonkeyObject.get("name").toString());
			System.out.println("Number Of Posts : " + jsonBlogObject.get("posts-total").toString());
			System.out.println();

			JSONArray jsonPostsArray = new JSONArray(jsonBlogObject.getJSONArray("posts").toString());
			System.out.println();

			for (int i = 0; i < jsonPostsArray.length(); i++) {

				JSONObject postsObject = new JSONObject(jsonPostsArray.get(i).toString());
				JSONArray jsonInnerArray = postsObject.getJSONArray("photos");
				int photosLength = jsonInnerArray.length();
				if (photosLength > 0) {
					System.out.print(" " + (i + 1) + ": ");
					for (int j = 0; j < photosLength; j++) {
						JSONObject imageObject = jsonInnerArray.getJSONObject(j);
						System.out.println(imageObject.get("photo-url-1280"));

					}

				} else {
					System.out.println(" " + (i + 1) + ": " + postsObject.get("photo-url-1280").toString());
				}

			}
		} catch (JSONException jsonexception) {
			jsonexception.printStackTrace();
		} catch (InputMismatchException exception) {
			System.out.println("please provide valid input range");

		}

	}

	public static void main(String[] args) {

		Blogs blogs = new Blogs();
		String jsonString = blogs.getJsonString();
		blogs.printValues(jsonString);

	}
}