package exceptions;

import java.util.Scanner;
import java.util.Random;

public class ATM {
	Scanner scan = new Scanner(System.in);
	private String atmName;
	private Long moneyInsideATM;
	private static final String HELPSTRING = "Transactions: exit, help, deposit, withdraw, balance";

	public ATM(String name, Long moneyInsideATM) {
		atmName = name;
		this.moneyInsideATM = moneyInsideATM;
	}

	private BankAccount getAccount(Bank bank) {
		return bank.account;
	}

	private int processTransaction(BankAccount account) {
		boolean moreTransactions = true;
		while (moreTransactions) {
			System.out.println("Enter transaction type ");
			String command = scan.next();
			if (command.equals("exit")) {
				System.out.println("Thanks for banking with us");
				moreTransactions = false;
				return 0;
				// System.exit(0);
			} else if (command.equals("help")) {
				System.out.println(HELPSTRING);
			} else if (command.equals("deposit")) {
				System.out.println("Enter Amount: ");
				int amount = scan.nextInt();
				account.deposit(amount);
			}

			else if (command.equals("withdraw")) {
				System.out.println("Enter Amount: ");
				int amount = scan.nextInt();
				if (this.moneyInsideATM < amount) {
					System.out.println("insufficient balance in ATM");
				} else {
					if (account.getBalance() < amount) {
						System.out.println("inssufficient fund in your account ");
					}

					else {
						account.withdraw(amount);
					}
				}

			}

			else if (command.equals("balance")) {
				System.out.println("Balance on account is " + account.getBalance());
			} else {
				System.out.println("sorry, unknown transaction");
			}
		}
		return 1;
	}

	public void startup(Bank bank) {
		System.out.println("Welcome to " + bank.bankName + " ATM " + atmName);
		Random random = new Random();
		int randomNumber = random.nextInt(6);
		boolean atmIsOpen = false;

		if (randomNumber == 3 | randomNumber == 5 | randomNumber == 2 | randomNumber == 0) {
			atmIsOpen = true;
			System.out.println("Connected to server");
		} else {
			System.out.println("Server Connnection failed ");
		}

		while (atmIsOpen) {
			BankAccount account = getAccount(bank);
			if (account != null) {
				System.out.println(HELPSTRING);
				int status = processTransaction(account);
				if (status == 0) {
					atmIsOpen = false;
				}
			} else {
				System.out.println("Good bye from the Virtual Bank");
				atmIsOpen = false;
			}
		}
	}
}
