package exceptions;

public class BankAccount

{

	private int balance;
	private Long accountNumber;
	private int pin;

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public int getPin() {
		return pin;
	}

	public void setPin(int pin) {
		this.pin = pin;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public BankAccount(int initialBalance, Long accountNumber, int pin) {
		this.accountNumber = accountNumber;
		this.pin = pin;
		this.deposit(initialBalance);
	}

	public void withdraw(int amount) {

		balance = balance - amount;
	}

	public void deposit(int amount) {

		balance = balance + amount;
	}

	public int getBalance() {
		return balance;
	}

}
