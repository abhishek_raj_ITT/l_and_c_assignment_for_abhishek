package exceptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Bank {
	public String bankName;
	public BankAccount account;
	public HashMap<Long, Integer> hm = new HashMap<Long, Integer>();
	Scanner scan = new Scanner(System.in);
	private ATM atm;
	private static final int INITIAL_BALANCE = 200;

	public Bank(String name) {

		this.hm.put((long) 11112222, 0000);
		bankName = name;
		atm = new ATM("SBI", (long) 1000000);

	}

	public void open(Bank bank) {
		ArrayList<Long> blockedAccounts = new ArrayList<Long>();

		while (true) {
			try {
				System.out.println("enter your account number");
				Long userAccount = Long.parseLong(scan.next());

				if (blockedAccounts.contains(userAccount)) {
					System.out.println("This Account has been blocked .Contact your bank");
				} else {
					if (this.hm.containsKey(userAccount)) {
						int passwordCount = 0;
						while (passwordCount < 3) {
							System.out.println("enter your password ");
							int password = scan.nextInt();
							if (this.hm.get(userAccount) == password) {
								account = new BankAccount(INITIAL_BALANCE, userAccount, password);
								atm.startup(bank);

							} else {
								passwordCount++;
							}
						}

						System.out.println("you card is blocked ");
						blockedAccounts.add(userAccount);

					}

					else {

						System.out.println("invalid account number");
					}
				}
			} catch (InputMismatchException ime) {
				System.out.println("please enter valid data ");
			} catch (NumberFormatException nfe) {

				System.out.println("account number should contains digits only");
			}
		}

	}
}
