package assignment5;

public class Customer {
	private String firstName;
	private String lastName;
	private Wallet myWallet;

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public float makePayment(float bill) {
		myWallet = new Wallet();
		float paidAmount = 0;
			if (myWallet.getTotalMoney() >= bill) {
				myWallet.subtractMoney(bill);
				paidAmount = bill;
			}
		return paidAmount;
	}
}