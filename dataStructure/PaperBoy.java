package assignment5;

import java.util.Scanner;

public class PaperBoy {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("enter bill to pay");
		float bill = scan.nextFloat();
		scan.close();
		Customer customer = new Customer();
		float paidAmount = customer.makePayment(bill);
		if (paidAmount == bill) {        
			 System.out.println("Payment done!. Thank you ");   
		} else {         
			System.out.println("Not enough balance to Pay");    
		}
	}
}
